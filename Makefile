input=./conf/main
output=./conf/tmp
final=./conf/out
lva-name::= $(shell cat ./opn/conf/name)
lva-nr::= $(shell cat ./opn/conf/nr)
lva-type::= $(shell cat ./opn/conf/type)

logfileName::= $(shell date +%F_%H_%M_%S).log
logfile::= $(output)/$(logfileName)

SHELL:=/bin/bash
#Zugriffsoptionen offen oder geschlossen
acess= opn cld
#Modus Prüfung (exm), Labor (lab), Skriptum 
mode= exm hlp lab skp ue
#Prüfungsmodus Beispiel (bsp), Mündlich (mdl), Multiple Choice (mpc), Theorie (thr), Text (txt)
type= bsp mdl mpc thr txt
#Sortierung nach Kapitel (chp), nach datum (date)
sorting= chp date
#Ausführung nur Angaben (ang), Lösungen (lsg), beides hybrid (hyb)
opt = ang lsg hyb
#Ausdruck optionen (cmp, so eng wie möglich oder pge, für jedes bsp eine eigene seite)
prt = cmp pge
#Seitenlayout für Einseitig oder Doppelseitig
page = one two
#Seitengeometrie maximum oder normal
geo = max avg
everything=$(foreach acess,$(acess),$(foreach mode,$(mode),$(foreach type, $(type),$(foreach opt,$(opt),$(foreach prt,$(prt),$(foreach page,$(page),$(foreach geo,$(geo),$(acess)-$(mode)-$(type)-$(opt)-$(prt)-$(page)-$(geo))))))))

.PHONY: all list test $(everything) clean update file

all:	clean $(everything)
	@gzip -9 $(logfile)
	@mv $(logfile).gz $(final)/

# Erstellt eine liste von allen Latex dokumenten welche gefunden werden und sortiere sie nach kapiteln
list:	update
	@$(foreach acess,$(acess),$(foreach mode,$(mode),$(foreach type,$(type), if [ -d "./$(acess)/$(mode)/$(type)" ] && [ $(acess) == opn ]; then find ./$(acess)/$(mode)/$(type)/ -name *.tex| sort -t '/' -k6,6 -n -k7,7 -n -k8,8 -n| awk '{printf "\\input{%s}\n", $$1}' > $(output)/$(acess)-$(mode)-$(type).tex; fi ; if [ -d "./$(acess)/$(mode)/$(type)" ] && [ $(acess) == cld ]; then find ./{opn,cld}/$(mode)/$(type)/ -name *.tex| sort -t '/' -k6,6 -n -k7,7 -n -k8,8 -n| awk '{printf "\\input{%s}\n", $$1}' > $(output)/$(acess)-$(mode)-$(type).tex; fi ; if [ ! -s "$(output)/$(acess)-$(mode)-$(type).tex" ] && [ -f "$(output)/$(acess)-$(mode)-$(type).tex" ] ; then rm $(output)/$(acess)-$(mode)-$(type).tex; fi; )))
# Erstellt ein pdf mit den datein, welche in den letzten 60 minuten bearbeitet wurden.
test:	clean
	@find $(foreach acess,$(acess),$(foreach mode,$(mode),$(foreach type,$(type),./$(acess)/$(mode)/$(type)/))) -name *.tex -mmin -60 | sort -t '/' -k6,6 -n -k7,7 -n -k8,8 -n| awk '{printf "\\input{%s}\n", $$1}' > $(output)/test.tex;
	@printf "\\input{$(input)/page/one.tex}" > $(output)/page.tex;
	@printf "\\input{$(input)/geo/max.tex}" > $(output)/geo.tex;
	@latexmk -pdf -synctex=1 -interaction=errorstopmode -output-directory='${output}' '${input}/test.tex';
	@mv ${output}/test.pdf $(final)

# Definiert alle möglichen Build optionen
define PROGRAMM_temp =
$(1)-$(2)-$(3)-$(4)-$(5)-$(6)-$(7): list
	@if [ -f "$(output)/$(1)-$(2)-$(3).tex" ]; then printf "\\input{$(output)/$(1)-$(2)-$(3).tex}" > $(output)/input.tex ; printf "Generating $(1)-$(2)-$(3)-$(4)-$(5)-$(6)-$(7) .... "; if [ -f "$(input)/page/$(6).tex" ]; then printf "\\input{$(input)/page/$(6).tex}" > $(output)/page.tex ; if [ -f "$(input)/geo/$(7).tex" ]; then printf "\\input{$(input)/geo/$(7).tex}" > $(output)/geo.tex ; latexmk -f -pdf -synctex=1 -interaction=nonstopmode -output-directory='${output}' '${input}/$(2)/$(3)/$(4)-$(5).tex' &>>$(logfile); mkdir -p $(final)/$(2)/$(3); mv ${output}/$(4)-$(5).pdf $(final)/$(2)/$(3)/$(lva-type)-$(lva-nr)-$(lva-name)-$(1)-$(2)-$(3)-$(4)-$(5)-$(6)-$(7).pdf; printf "Done\n"; else printf "\nGeometry describing file not found at ./conf/main/geo/\n" ;fi; else printf "\nPage describing file not found at ./conf/main/page/\n" ; fi; fi;

endef

$(foreach acess,$(acess),$(foreach mode,$(mode),$(foreach type, $(type),$(foreach opt,$(opt),$(foreach prt,$(prt),$(foreach page,$(page),$(foreach geo,$(geo),$(eval $(call PROGRAMM_temp,$(acess),$(mode),$(type),$(opt),$(prt),$(page),$(geo))))))))))

# Sauber machen
clean:
	@find opn/ -mindepth 1 -type d -empty -delete
	@find cld/ -mindepth 1 -type d -empty -delete
	@rm -f $(output)/*.{tex,ps,pdf,fls,fdb,log,aux,out,dvi,bbl,blg,toc,synctex.gz}*
	@rm -rf $(final)/*
	
# Neue README, TODO, und LICENSE files in die repos kopieren
update:
	@shopt -s dotglob; for file in ./*; do if [ -f "$$file" ]  && [[ "$$file" != *.git ]] && [[ "$$file" != *Makefile ]] && [[ "$$file" != *.gitmodules ]]; then rm $$file; fi; done;
	@shopt -s dotglob; cp ./conf/init/opn/* ./
	@shopt -s dotglob; for file in ./cld/*; do if [ -f "$$file" ]  && [[ "$$file" != *.git ]] && [[ "$$file" != *Makefile ]] && [[ "$$file" != *.gitmodules ]]; then rm $$file; fi; done;
	@shopt -s dotglob; if [ -d "./cld" ]; then cp ./conf/init/cld/* ./cld/; fi;

# Leere Dateistruktur erstellen um einfach neue inhalte hinzufügen zu können
file:
	@mkdir -p {opn,cld}/{exm,hlp,lab,skp,ue}/{bsp,mdl,mpc,thr,txt}/{chp,date,misc}

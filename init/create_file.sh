#!/bin/bash

# Erstelle automatisch eine Ordnerstruktur um dann darin Beispiele abzuspeichern
# $1 acess ( opn cld)
# $2 mode (exm lab hlp skp ue)
# $3 type (bsp mdl mpc thr txt)
# $4 Anzahl der Kapiteln
# $5 Maximalanzahl der Fragen (überflüssige Fragen müssen gelöscht werden)

init_struct(){
# Erstelle den Subordner mit Fragenordner
mkdir -p ./$1/$2/$3/chp/$4/$5

# Erstelle eine Latex Datei
echo -e '\\begin{question}[section='"$4,name={},difficulty=,mode=$2,type=$3,tags={}]\n\n"'\\end{question}'"\n"'\\begin{solution}'"\n\n"'\\end{solution}' > $1/$2/$3/chp/$4/$5/$4-$5.tex

}

for ((count_chp=1; count_chp <=$4; count_chp++))
do
  for ((count_question=1; count_question <=$5; count_question++))
  do
  init_struct $1 $2 $3 $count_chp $count_question
  done
done

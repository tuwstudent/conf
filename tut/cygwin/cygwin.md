# Unterlagen auf Windows kompilieren (Workaround)

# Inhaltsverzeichnis

1. [Einleitung](#1-einleitung)
1. [Cygwin](#2-cygwin)
1. [TeX Live](#3-tex-live)
1. [Generieren von PDFs via cygwin und Makefile](#4-generieren-von-pdfs-via-cygwin-und-makefile)

# 1 Einleitung

Die LVA-Repositories erlauben das Generieren von diversen Variationen von Sammel-PDFs über ein Makefile. Windows selbst kann mit Makefiles leider nicht umgehen, während es unter Linux problemlos möglich ist. Diese Anleitung zeigt einen Workaround, mit dem man auch in einer Windows-Umgebung die PDFs generieren kann.

Voraussetzungen:

* Etwa 3 GB freier Speicherplatz
* Windows Vista oder neuer, 32-bit oder 64-bit 

# 2 Cygwin

Mit Cygwin lassen sich unter Anderem Unix-Befehle auf Windows ausführen.

## 2.1 Installation

1. Direkt von der [cygwin Homepage](https://www.cygwin.com/) die [setup-x86_64.exe](https://www.cygwin.com/setup-x86_64.exe) (64-bit-Systeme) oder die [setup-x86.exe](https://www.cygwin.com/setup-x86.exe) (32-bit-Systeme) herunterladen und ausführen.

1. `Install from Internet` wählen und auf `Weiter` klicken.

1.  Bei _Root Directoy_ das Installationsverzeichnis für cygwin wählen. Aufgrund der späteren TeX Live-Installation sollten bis zu 3 GB Platzbedarf eingerechnet werden.  
Bei _Install For_ wählen ob man cygwin für alle Nutzer des PCs oder nur den aktuellen installieren will. In den meisten Fällen sollte man es bei _All Users_ belassen.  
Auf `Weiter` klicken.

1. Bei _Local Package Directory_ einen Ordner für die Packages (Linux-Programme oder Befehle) wählen. Es kann ein beliebiger Ordner gewählt werden, ich habe als Installationsordner `D:\Programme\cygwin\` und für die Packages einen Ordner `D:\Programme\cygwinPackages\` angelegt.  
Auf `Weiter` klicken.

1. Proxy wählen. Wenn unklar, auf `Use System Procy Settings` belassen und auf `Weiter` klicken.

1. Einen Download-Server für die packages wählen.  Ich empfehle `http://mirror.easyname.at`. Findet man knapp über der Mitte in der Liste, alternativ kann man auch die URL hier kopieren und einfügen.  
Auf `Weiter` klicken.

1. Nun muss man die konkreten Packages wählen, die installiert werden sollen. Bei _View_ wählt man `Not Installed`, sucht direkt rechts daneben bei _Search_ nach dem gewünschten Package, wählt es aus der Liste aus und in der Spalte _New_ ändert man durch Anklicken den Status des Packages `Skip` auf die neueste Version (der unterste Eintrag).  
Visuelle Hilfe:  
![cygwin-Packages Suche und Installation](tut/cygwin/img/cygwinPackages.gif)

	Wir benötigen folgende Packages:
	* **make** [zum Ausführen der Makefiles]
	* **perl** [zum Ausführen des Installationsscripts von TeX Live]
	* **wget** [zum Herunterladen von Dateien in der Shell bzw. Installieren von TeX Live]
	* **fontconfig** [von TeX Live benötigt]
	* **ghostscript** [von TeX Live benötigt]
	* **libXaw7** [von `xdvi` bzw. Tex Live benötigt]
	* **ncurses** [für den `clear`-Befehl des TeX Live-Installationsscripts benötigt].  _Falls ncurses nicht aufscheint ist es schon zum Installieren ausgewählt_

	Wenn alle Packages ausgewählt wurden auf `Weiter` klicken.

1. Nochmal auf `Weiter` klicken.

1. cygwin wird nun heruntergeladen und installiert. Falls man Packages aktualisieren oder ändern möchte, muss man wieder die _setup-x86_64.exe_ bzw. die _setup-x86.exe_ aufrufen und die Schritte durchlaufen. Die vorher gewählten Pfade und Einstellungen sollten erkannt werden, entscheident ist also nur Schritt 7.

1. Nach der Installation kann man noch wahlweise ein Desktop-Icon und ein Startmenü-Icon hinzufügen. Ich empfehle letzteres.

Cygwin ist nun installiert und einsatzbereit. Um die PDFs zu generieren brauchen wir aber auch noch eine TeX Installation. Auf Windows wird meistens MiKTeX verwendet, cygwin ist aber wie oben erwähnt für Unix/Linux-Befehle gemacht. Die einfachste Variante ist, direkt in cygwin TeX Live zu installieren.

# 3 TeX Live

TeX Live ist eine TeX-Distribution und erlaubt das Kompilieren von TeX-Dateien. Man kann damit also aus TeX-Dateien z.B. PDFs erstellen.

## 3.2 Installation

Damit cygwin auf TeX Live zugreifen kann müssen wir es direkt darin installieren. Eine eventuell bereits auf Windows installierte TeX Live-Distribution ist - wenn überhaupt - nur mit viel Aufwand nutzbar.

1. Von der [TeX Live Homepage](https://tug.org/texlive/acquire-netinstall.html) die [install-tl.zip](http://mirror.ctan.org/systems/texlive/tlnet/install-tl.zip) herunterladen.

1. Entpackt die ZIP-Datei an einen euch bekannten Ort. Wir werden den Pfad später brauchen

1. Cygwin Terminal starten. Falls man vorher ein Startmenü-Icon hinzugefügt hat, findet man es unter dem Namen `Cygwin64 Terminal`. Alternativ findet man die exe auch in `[Pfad zum Installationsordner]\bin\mintty.exe`. In unserem Beispiel wäre das `D:\Programme\cygwin\bin\mintty.exe`.

1. Im Terminal gibt man nun ``cd /cygdrive/c`` ein, wobei `c` mit dem Laufwerksbuchstaben zu ersetzen ist, wo ihr die ZIP-Datei entpackt habt. Also für D:/ `cd /cygdrive/D` usw.

1. Nun navigiert man zum vorhin entpackten Ordner. Hierbei nützliche Befehle:
	* `ls` zeigt eine Liste der Ordner und Dateien am aktuellen Pfad an
	* `cd [Ordner oder Pfad]` wechselt in einen Ordner oder den angegebenen Pfad.
	* `cd ..` wechselt in den übergeordneten Ordner (bzw. einen Ordner zurück)
	* Mit der _Tabulator-Taste_ kann man Ordner- und Datinamen vervollständigen. Meist reichen die ersten zwei oder drei Buchstaben dafür

	Wenn man im Ordner ist, sollte man mit `ls` prüfen, ob man die gleichen Dateien wie hier sieht:  
	![Ordnerinhalt](tut/cygwin/img/ls.png)
	
1. Nun startet man die Installation von TeX Live mit `./install-tl` und `Enter`.

1. Durch Eingabe von `O` und `Enter` die Optionen aufrufen.

1. Durch Eingabe von `L` und `Enter` Symlinks erschaffen. Anschließend werden neue Werte für die Pfade erfragt. Alle drei ohne weitere Eingaben mit `Enter` bestätigen. Die Optionen sollten nun so aussehen:  
![TeX Live Optionen](tut/cygwin/img/optionen.png)

1. Durch Eingabe von `R` und `Enter` zurück ins Hauptmenü.

1. Durch Eingabe von `C` und `Enter` in die zu installierenden Pakete wechseln.  
Hier gibt es nun einige Optionen, die durch den jeweils links stehenden Buchstaben und `Enter` an- und abwählbar sein sollten. Ich empfehle folgende Auswahl:  
![TeX Live Pakete](tut/cygwin/img/pakete.png)  
Man kann auch eine minimale Installation wählen und die restlichen Pakete über den inkludierten _tlmgr_ installieren, um Speicherplatz zu sparen. Das ist aber nur für erfahrene TeX Live-Nutzer zu empfehlen.

1. Durch Eingabe von `R` und `Enter` zurück ins Hauptmenü.

1. Durch Eingabe von `I` und `Enter` die Installation starten. Dies dauert jetzt einige Zeit (~50 min). In dieser Zeit kann man den cygwin Terminal minimieren.  

1. Nach dem Ende der Installation sollte man noch mit `pdftex -v` prüfen, ob TeX Live korrekt installiert wurde. Wenn keine Fehlermeldung erscheint, kann man ab jetzt über den cygwin Terminal in den jeweiligen LVA-Repository-Ordnern über das Makefile PDFs generieren.

1. (Stand Februar 2019). In der neuesten TeX Live Distribution ist im Package exsheets ein Fehler, der das Generieren von PDFs verhindert. [Hier](https://bitbucket.org/cgnieder/exsheets/issues/44/error-latex3-error-variant-form-n#comment-50468631) ist das zugehörige Issue inklusive einem Workaround. Es kann sein, dass mittlerweile der Fehler behoben wurde und dieser Schritt ignoriert werden kann. Einfach ins Issue nach dem aktuellen Stand schauen.

# 4 Generieren von PDFs via cygwin und Makefile

Leider ist hier auch noch ein kleiner Workaround nötig.

Im jeweiligen (lokalen) Repository-Ordner muss das Makefile von `./conf/Makefile` nach `./` kopiert werden, also ins Hauptverzeichnis vom Repository. _Hierbei wird die Dateistruktur natürlich verändert. Falls man (später einmal) einen commit machen möchte, darf das veränderte Makefile nicht stagen! Wenn man nur PDFs generieren möchte ist es egal._

1. Navigiere im cygwin Terminal zu dem Ordner, wo die Inhalte des Repositories entpackt oder geclont wurden.

1. Mit `make all` werden nun alle Varianten von PDFs generiert. Diese findet man anschließend in `./conf/out/`. Sie folgen der Ordnerstruktur in den `opn` bzw. `cld`-Ordnern. Beispiel: Die PDFs für die mündliche Prüfung findet man in `./conf/out/exm/mdl/`

Sollte eine Fehlermeldung `Makefile:1: *** Fehlender Trenner.  Schluss.` erscheinen, wurde entweder vergessen, das Makefile aus dem conf Ordner ins Hauptverzeichnis zu kopieren, oder im Pfad befinden sich Leerzeichen. Im zweiten Fall entweder die Leerzeichen der übergeordneten Ordner z.B. durch Bindestriche oder Unterstriche ersetzen oder den Ordner vom Repository temporär an eine andere Stelle ohne Leerzeichen im Pfad verschieben. Nach dem Generieren der PDFs kann man den Ordner wieder an seinen alten Platz verschieben.